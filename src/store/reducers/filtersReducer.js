import produce from 'immer'

const actionTypes = {
	SET_CURRENT_LEVEL: 'SET_LEVEL',
	SET_CURRENT_TAGS: 'SET_TAGS',
	SET_LEVELS_LIST: 'SET_LEVELS_LIST',
	SET_TAGS_LIST: 'SET_TAGS_LIST',
}

export const actionsCreators = {
	setCurrentLevel: (payload) => ({
		type: actionTypes.SET_CURRENT_LEVEL,
		payload,
	}),
	setCurrentTags: (payload) => ({
		type: actionTypes.SET_CURRENT_TAGS,
		payload,
	}),
	setLevelsList: (payload) => ({ type: actionTypes.SET_LEVELS_LIST, payload }),
	setTagsList: (payload) => ({ type: actionTypes.SET_TAGS_LIST, payload }),
}

export const stateShape = {
	current: { level: null, tags: [] },
	levels: [],
	tags: [],
}
const filtersReducer = produce((draft, { type, payload }) => {
	switch (type) {
		case actionTypes.SET_CURRENT_LEVEL:
			draft.current.level = payload
			return
		case actionTypes.SET_CURRENT_TAGS:
			draft.current.tags = [...new Set([...draft.current.tags, ...payload])]
			return
		case actionTypes.SET_LEVELS_LIST:
			draft.levels = payload
			return
		case actionTypes.SET_TAGS_LIST:
			draft.tags = payload
			return
		default:
			return
	}
}, stateShape)

export default filtersReducer
