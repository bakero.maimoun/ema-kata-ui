export const baseURL = process.env.REACT_APP_BASE_URL

const endpoints = {
	videos: 'v1/videos',
	levels: 'v1/referential/levels',
	tags: 'v1/referential/tags',
}

export default endpoints
