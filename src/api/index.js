import axios from 'axios'
import { formatOptions, formatVideos } from './formatters'
import endpoints, { baseURL } from './endpoints'
import { queryString } from '../utils'

export const fetch = async (
	url,
	{ query = {}, name, defaultValue, formatter }
) => {
	const [data, error] = await axios
		.get(`${baseURL}${url}?${queryString(query)}`)
		.then((response) => [response.data, null])
		.catch((err) => [null, err])

	if (error) {
		console.log(`error while fetching ${name || ''} >>`, error.toJSON())
		return defaultValue || null
	}

	try {
		if (formatter) return formatter(data)
		return data
	} catch (err) {
		console.log(`error while formatting ${name || ''} >>`, error.toJSON())
	}
}

export const populateDB = async (payload = []) => {
	const data = await fetchVideos()

	// TODO: optimize this command condition
	if (data.length <= payload.length) {
		const [response, error] = await axios
			.post(`${baseURL}${endpoints.videos}`, payload)
			.then((response) => [response.data, null])
			.catch((err) => [null, err])

		if (error) {
			console.log(`error while posting videos >>`, error.toJSON())
			return null
		}
		return response
	}

	return null
}

export const fetchVideos = async (tags = [], level) =>
	await fetch(endpoints.videos, {
		query: { level: level?.value, tag: tags.map((elt) => elt?.value) },
		name: 'videos',
		defaultValue: [],
		formatter: formatVideos,
	})

export const fetchLevels = async () =>
	await fetch(endpoints.levels, {
		name: 'levels',
		defaultValue: [],
		formatter: formatOptions,
	})

export const fetchTags = async () =>
	await fetch(endpoints.tags, {
		name: 'tags',
		defaultValue: [],
		formatter: formatOptions,
	})
