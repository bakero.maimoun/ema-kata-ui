import qs from 'qs'

const extractVideoId = (url) => {
	const [, query] = url.split('?')
	return qs.parse(query).v
}

export const formatVideos = (videos = []) =>
	videos.map((video) => ({
		...video,
		embedUrl: `https://www.youtube.com/embed/${extractVideoId(video.url)}`,
		thumbnail: `https://img.youtube.com/vi/${extractVideoId(video.url)}/0.jpg`,
	}))

export const formatOptions = (options = []) =>
	options.map((elt) => ({ label: elt, value: elt }))
