import qs from 'qs'

export const queryString = (query) =>
	qs.stringify(query, { arrayFormat: 'repeat' })
