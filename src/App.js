import { Fragment, useCallback, useEffect, useReducer, useState } from 'react'

import * as api from './api'
import mocks from './mocks'

import VideoEmbed from './components/VideoEmbed/index'
import Template from './components/Template'
import NavBar from './components/NavBar'
import Thumbnail from './components/Thumbnail'
import Filters from './components/Filters'
import Maybe from './components/Maybe'

import filtersReducer, {
	actionsCreators as ac,
	stateShape,
} from './store/reducers/filtersReducer'

import './App.css'

const actionsHandles = {
	tags: ac.setCurrentTags,
	level: ac.setCurrentLevel,
}

function App() {
	const [videos, setVideos] = useState([])
	const [current, setCurrent] = useState(null)

	const [autoplay, setAutoPlay] = useState(false)
	const [loading, setLoading] = useState(0)

	const [{ levels, tags, current: filters }, dispatch] = useReducer(
		filtersReducer,
		stateShape
	)

	useEffect(() => {
		async function handleRetrieveTags() {
			const tags = await api.fetchTags()
			dispatch(ac.setTagsList(tags))
		}
		async function handleRetrieveLevels() {
			const levels = await api.fetchLevels()
			dispatch(ac.setLevelsList(levels))
		}

		async function initApp() {
			const videos = await api.populateDB(mocks)
			if (videos?.length > 0) setVideos(videos)
		}
		initApp()
		handleRetrieveLevels()
		handleRetrieveTags()
	}, [])

	useEffect(() => {
		async function handleFetchVideos() {
			setLoading((prv) => prv + 1)
			const { tags, level } = filters
			const data = await api.fetchVideos(tags, level)
			setVideos(data)
			if (data.length > 0) setCurrent(data[0])
			setLoading((prv) => prv - 1)
		}
		handleFetchVideos()
	}, [filters])

	const handleClick = useCallback(
		(id) => {
			const selected = videos.find((elt) => elt.id === id)
			setCurrent(selected)
			setAutoPlay(true)
		},
		[videos]
	)

	const handleFiltersChange = useCallback(
		(key, value) => {
			dispatch(actionsHandles[key](value))
		},
		[dispatch]
	)

	return (
		<Template navBar={<NavBar />}>
			<Filters onChange={handleFiltersChange} params={{ levels, tags }} />
			<VideoEmbed video={current} autoplay={autoplay} />
			<Maybe loading={loading !== 0} collection={videos}>
				<Fragment>
					{videos.map((video) => (
						<Thumbnail key={video.id} {...video} onClick={handleClick} />
					))}
				</Fragment>
			</Maybe>
		</Template>
	)
}

export default App
