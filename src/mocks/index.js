const mocks = [
	{
		title: 'Le Comptoir OCTO - Devenir une entreprise agile',
		description:
			'Une entreprise agile est “une entreprise capable de mobiliser son intelligence collective pour',
		url: 'https://www.youtube.com/watch?v=MNjPfMk8CZY',
		duration: 19.29,
		level: 'EASY',
		tags: [
			'agile',
			'transformation',
			'entreprise',
			'management',
			'leadership',
			'ESN',
			'OCTO Technology',
			'Ludovic Cinquin',
		],
	},
	{
		title: 'Le Comptoir OCTO - Accelerate x Engie',
		description:
			"Constituée d'une dizaine de plate-formes digitales, Engie Digital a mis en place une démarche",
		url: 'https://www.youtube.com/watch?v=oBPs4P_kaLI',
		duration: 19.29,
		level: 'MEDIUM',
		tags: [
			'comptoir',
			'engie',
			'engie digital',
			'accelerate',
			'agile',
			'devops',
		],
	},
	{
		title: 'Culture Flow - Bonus : Comment booster son flux',
		description:
			'Dans ce nouvel épisode, nous vous proposons de vous présenter comment booster votre flux.',
		url: 'https://www.youtube.com/watch?v=fnoF1kQfFOo',
		duration: 19.29,
		level: 'HARD',
		tags: [
			'flow',
			'agile',
			'produit',
			'développement',
			'organisation',
			'méthodologie',
			'design',
			'accelerate',
			'devops',
			'UXdesign',
			'OKR',
			'DDD',
			'team topologies',
		],
	},
	{
		title: 'Culture Flow - Épisode 6 :  Les conditions nécessaires',
		description:
			"Dans ce nouvel épisode, nous vous proposons de vous présenter les conditions nécessaires à l'approche Flow.",
		url: 'https://www.youtube.com/watch?v=nwXFXQ7-BMc',
		duration: 19.29,
		level: 'HARD',
		tags: [
			'flow',
			'agile',
			'produit',
			'développement',
			'organisation',
			'méthodologie',
			'design',
			'accelerate',
			'devops',
			'UXdesign',
			'OKR',
			'DDD',
			'team topologies',
		],
	},
]

export default mocks
