import Loading from './Loading'
import Empty from './Empty'

import useDebounce from '../../hooks/useDebounce'

export default function Maybe({
	loading,
	empty,
	collection,
	children,
	delay = 100,
}) {
	const debouncedLoading = useDebounce(loading, 100)
	const debouncedEmpty = useDebounce(empty || collection?.length === 0, delay)

	if (debouncedLoading) return <Loading />
	if (debouncedEmpty) return <Empty text='no result found' />

	return children
}
