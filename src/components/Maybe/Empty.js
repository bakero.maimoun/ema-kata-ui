import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
	root: {
		display: 'flex',
		alignItems: 'center',
		alignContent: 'center',
		justifyContent: 'center',
		width: '90%',
		height: '90%',
		border: '1px solid white',
		borderRadius: 8,
	},
}))

export default function Empty({ text }) {
	const classes = useStyles()

	return <div className={classes.root}>{text}</div>
}
