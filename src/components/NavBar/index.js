import { makeStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'

const useStyles = makeStyles((theme) => ({
	root: {
		width: '100%',
		position: 'absolute',
		top: 0,
	},
	title: {
		display: 'none',
		[theme.breakpoints.up('sm')]: {
			display: 'block',
		},
	},
}))

const NavBar = ({ filters }) => {
	const classes = useStyles()

	return (
		<AppBar position='sticky' color='secondary' className={classes.root}>
			<Toolbar variant='dense'>
				<Typography className={classes.title} variant='h6' noWrap>
					EMA KATA
				</Typography>
				{filters}
			</Toolbar>
		</AppBar>
	)
}

export default NavBar
