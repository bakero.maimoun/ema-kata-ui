import Grid from '@material-ui/core/Grid'
import { makeStyles } from '@material-ui/core/styles'

import '../../App.css'

const useStyles = makeStyles((theme) => ({
	'@global': {
		'*::-webkit-scrollbar': {
			width: '0.4em',
		},
		'*::-webkit-scrollbar-track': {
			'-webkit-box-shadow': 'inset 0 0 6px rgba(0,0,0,0.00)',
		},
		'*::-webkit-scrollbar-thumb': {
			backgroundColor: 'rgba(0,0,0,.1)',
			outline: '1px solid slategrey',
		},
	},
}))
const Template = ({ children, navBar }) => {
	useStyles()

	return (
		<div className='App'>
			<header className='App-header'>
				{navBar}
				{children[0]}
				<Grid container justify='center' className='body'>
					<Grid item xs={9}>
						{children[1]}
					</Grid>
					<Grid item xs={3} className='scroller'>
						{children[2]}
					</Grid>
				</Grid>
			</header>
		</div>
	)
}

Template.defaultProps = {
	children: [],
}

export default Template
