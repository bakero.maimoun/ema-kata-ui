import { createMuiTheme } from '@material-ui/core/styles'
import { ThemeProvider } from '@material-ui/styles'

const darkTheme = createMuiTheme({
	palette: {
		type: 'dark',
	},
})

export default function Theme({ children }) {
	return <ThemeProvider theme={darkTheme}>{children}</ThemeProvider>
}
