const VideoEmbed = ({ video, width, height, autoplay }) => {
	if (!video?.embedUrl) return null

	const src = `${video.embedUrl}?${autoplay ? 'autoplay=1' : ''}`

	return (
		<iframe
			width={width}
			height={height}
			src={src}
			frameBorder='0'
			allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
			allowFullScreen
			title='Embedded youtube'
		/>
	)
}

VideoEmbed.defaultProps = {
	url: null,
	width: 700,
	height: 400,
}

export default VideoEmbed
