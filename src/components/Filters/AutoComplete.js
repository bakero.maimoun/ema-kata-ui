import Autocomplete from '@material-ui/lab/Autocomplete'
import { makeStyles } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import { useCallback } from 'react'

const useStyles = makeStyles((theme) => ({
	root: {
		width: '100%',
		'& > * + *': {
			marginTop: theme.spacing(3),
		},
	},
}))

export default function AutoComplete({
	options,
	multiple,
	defaultValue,
	label,
	placeholder,
	onChange,
}) {
	const classes = useStyles()

	const handleChange = useCallback(
		(evt, values) => {
			evt.stopPropagation()
			if (!multiple) onChange([values])
			onChange(values)
		},
		[onChange, multiple]
	)

	return (
		<div className={classes.root}>
			<Autocomplete
				onChange={handleChange}
				multiple={multiple}
				id='tags-outlined'
				options={options}
				getOptionLabel={(option) => option.label}
				defaultValue={defaultValue}
				filterSelectedOptions
				renderInput={(params) => (
					<TextField
						{...params}
						variant='outlined'
						label={label}
						placeholder={placeholder}
						InputLabelProps={{ shrink: true }}
					/>
				)}
			/>
		</div>
	)
}
