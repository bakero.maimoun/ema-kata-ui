import { useCallback } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'

import AutoComplete from './AutoComplete'

const useStyles = makeStyles((theme) => ({
	root: {
		marginTop: 100,
		border: '1px solid white',
		borderRadius: 8,
		padding: '20px 30px',
	},
}))

const Filters = ({ onChange, params }) => {
	const classes = useStyles()

	const handleChange = useCallback(
		(key) => (value) => onChange(key, value),
		[onChange]
	)

	return (
		<Grid container justify='center' spacing={4}>
			<Grid
				item
				xs={11}
				justify='space-between'
				container
				className={classes.root}
			>
				<Grid item xs={8}>
					<AutoComplete
						onChange={handleChange('tags')}
						options={params.tags}
						multiple
						label='tags'
						placeholder='tags-placeholder'
					/>
				</Grid>
				<Grid item xs={3}>
					<AutoComplete
						onChange={handleChange('level')}
						options={params.levels}
						label='level'
						placeholder='level-placeholder'
					/>
				</Grid>
			</Grid>
		</Grid>
	)
}

export default Filters
