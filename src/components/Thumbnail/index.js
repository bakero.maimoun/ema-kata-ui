import React, { useCallback } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'

const useStyles = makeStyles({
	root: {
		maxWidth: 345,
	},
	media: {
		height: 140,
	},
})

function Thumbnail({ id, title, thumbnail, onClick }) {
	const classes = useStyles()

	const handleClick = useCallback(() => onClick(id), [id, onClick])

	return (
		<Card onClick={handleClick} className={classes.root}>
			<CardActionArea>
				<CardMedia className={classes.media} image={thumbnail} title={title} />
				<CardContent>
					<Typography gutterBottom variant='h5' component='h2'>
						{title}
					</Typography>
				</CardContent>
			</CardActionArea>
		</Card>
	)
}

export default Thumbnail
