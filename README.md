# EMA KATA APP

Youtube Video Player Test

### Setup and Installation
This project is a javascript based code, it uses mainly Yarn as a package manager and
react as ui library.
####Prerequisites
* Node Js
* Yarn

#### Installing
after you have finished installtion requirements and you have a copy clone of the repository,
you can start the application in three steps:

run ` yarn install ` to install dependencies, once it finished run `yarn start`
and then place yourself in a browser and open [http://localhost:3000][app_home]

**CAUTION!** If you want to use npm to run the app, you're free to go,
but you will end up with two package managers in your copy.

### Deploying

#### Locally
first you have to build the package to generate an optimized production build
#### `npm run build`
then you can use the generated `./build` directory to deploy the app in a web
server such as **Nginx or Apache**, follow this
[instructions][deploy_link]
if you want o do so.

### Usage
`/api/endpoints` all api endpoints

`/api/formatters` function utilities to format data 

`/api/index` data fetching services

`/components` common & business components

`/mocks` mocks to run the app (change the content & reload the app to populate DB with new content)

### Authors
* **BAKR MAIMOUN**
